# README #
## Приложение по работе со справочником устройств оповещения ##


### Краткое описание ###
Приложение по работе со справочником устройств оповещения.       
Проект состоит из веб приложения на основе Django с возможностью просматривать все устройства и формой добавления нового, а также  из открытого API на основе Django Rest Framework и полным CRUD функционалом для устройств c возможностью поиска и фильтрации.    
     
Каждое устройство имеет поля:          

*  Тип устройства (сирена или громкоговоритель)
*  Адрес размещения (строка)
*  Широта (число с 6 знаками после запятой)
*  Долгота (число с 6 знаками после запятой)
*  Радиус зоны звукопокрытия (целое число в метрах)


### Полезные ссылки ###
+ [Django documentation](https://docs.djangoproject.com/en/2.2/)
+ [Django rest framework](https://www.django-rest-framework.org/)




### Requirements ###

* dj-database-url==0.5.0
* Django==3.0.3
* django-filter==2.2.0
* django-heroku==0.3.1
* djangorestframework==3.11.0
* gunicorn==20.0.4
* psycopg2==2.8.4
* psycopg2-binary==2.8.4
* pylint==2.4.4
* sqlparse==0.3.0
* typed-ast==1.4.1
* whitenoise==5.0.1
* wrapt==1.11.2

### Сборка и запуск ###

```bash
git clone git@bitbucket.org:gladunvv/reference-book-app.git
cd reference-book-app
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
cd mysite/
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```


### Особенности ###
Приложение расположенно на https://refbooktest.herokuapp.com   
Тип устройства в базу записывается с помощью int choice, где 1 - Серена а 2 - Громкоговоритель.       
Фильтрация может быть по **адресу**, **типу устройства** и **зоне звукового покрытия**.     
Поиск может производиться по **адресу**, **долготе** и **широте**.    
Форма добавления нового устройства выполнена с помощью AJAX.     
К приложению написанны тесты.        



### Примеры запросов и ответов ###


#### Endpoint 1: ####
> GET https://refbooktest.herokuapp.com/apibook/v1/device

```json
[
    {
        "id": 7,
        "device_type": 2,
        "address": "Казань, Русско-Немецкая Швейцария",
        "latitude": "55.802667",
        "longitude": "49.162685",
        "sound_cover_zone": 500
    },
    {
        "id": 6,
        "device_type": 1,
        "address": "Чита, парк культуры и отдыха имени Победы",
        "latitude": "52.046528",
        "longitude": "113.472184",
        "sound_cover_zone": 989
    },
    
    ...
]
```

#### Endpoint 2: ####
> POST https://refbooktest.herokuapp.com/apibook/v1/device   

Тело запроса:
```json
{
    "device_type": 2,
    "address": "Лукоморье, Дуб зелёный",
    "latitude": "55.802667",
    "longitude": "49.162685",
    "sound_cover_zone": 999
}
```
Тело ответа:
```json
{
    "id": 9,
    "device_type": 2,
    "address": "Лукоморье, Дуб зелёный",
    "latitude": "55.802667",
    "longitude": "49.162685",
    "sound_cover_zone": 999
}
```

#### Endpoint 3: ####
>PUT https://refbooktest.herokuapp.com/apibook/v1/device?device=9      

Тело запроса:
```json
{
    "device_type": 1,
    "address": "Лукоморье, Дуб спилили",
    "latitude": "0.111111",
    "longitude": "0.111111",
    "sound_cover_zone": 123
}
```
Тело ответа:

```json
{
    "id": 9,
    "device_type": 1,
    "address": "Лукоморье, Дуб спилили",
    "latitude": "0.111111",
    "longitude": "0.111111",
    "sound_cover_zone": 123
}
```

#### Endpoint 4: ####
>DELETE https://refbooktest.herokuapp.com/apibook/v1/device?device=9 

Тело ответа:
```json
{
    "message": "Device 1 with longitude: 0.111111 and latitude 0.111111 deleted successfully"
}
```   

#### Endpoint 5: ####
> GET https://refbooktest.herokuapp.com/apibook/v1/filter?device_type=1       

Тело ответа:       

```json
[
    {
        "id": 6,
        "device_type": 1,
        "address": "Чита, парк культуры и отдыха имени Победы",
        ...
    },
    {
        "id": 5,
        "device_type": 1,
        "address": "Санкт-Петербург, Петропавловская крепость",
        ...
    },
    {
        "id": 3,
        "device_type": 1,
        "address": "Хабаровск, улица Льва Толстого, 2А",
        ...
    }
]
```

#### Endpoint 6: ####
> GET https://refbooktest.herokuapp.com/apibook/v1/search?search=Москва        

Тело ответа:    
```json
[
    {
        "id": 4,
        "device_type": 2,
        "address": "Москва, Большой Кремлёвский сквер",
        "latitude": "55.751222",
        "longitude": "37.620315",
        "sound_cover_zone": 777
    }
]
```



### License ###
This project is licensed under the terms of the MIT license