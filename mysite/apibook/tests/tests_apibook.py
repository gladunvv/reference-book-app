from django.test import TestCase
from django.urls import reverse

from rest_framework.test import APIClient
from rest_framework import status

from refbook.models import NotificationDevice


SEARCH_DEVICE_URL = reverse('apibook:search')
FILTER_DEVICE_URL = reverse('apibook:filter')
DEVICE_URL = reverse('apibook:device')


def sample_device_refbook():

    data_device = [
        {
            'device_type': 1,
            'address': 'address1',
            'latitude': 1.111123,
            'longitude': 1.333123,
            'sound_cover_zone': 111,
        },
        {
            'device_type': 2,
            'address': 'address2',
            'latitude': 0.111111,
            'longitude': 3.111111,
            'sound_cover_zone': 222,
        },
        {
            'device_type': 1,
            'address': 'address3',
            'latitude': 3.333333,
            'longitude': 4.444444,
            'sound_cover_zone': 333,
        },
    ]

    for data in data_device:
        NotificationDevice.objects.create(**data)


class RefbookApiTests(TestCase):

    def setUp(self):
        self.client = APIClient()

    def test_search_book(self):
        sample_device_refbook()
        res = self.client.get(SEARCH_DEVICE_URL + '?search=address2')
        self.assertEqual(res.data[0]['sound_cover_zone'], 222)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_filter_book(self):
        sample_device_refbook()
        res = self.client.get(FILTER_DEVICE_URL + '?device_type=1')
        self.assertEqual(len(res.data), 2)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_device_get(self):
        sample_device_refbook()
        res = self.client.get(DEVICE_URL)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_device_post(self):
        context = {
            'device_type': 1,
            'address': 'address1',
            'latitude': 1.111123,
            'longitude': 1.333123,
            'sound_cover_zone': 111,
        }
        res = self.client.post(DEVICE_URL, context)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

    def test_device_put(self):
        sample_device_refbook()
        context = {
            'device_type': 1,
            'address': 'addressOK',
            'latitude': 1.111123,
            'longitude': 1.333123,
            'sound_cover_zone': 111,
        }
        res = self.client.put(DEVICE_URL + '?device=1', context)
        device = NotificationDevice.objects.get(pk=1)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(device.address, 'addressOK')

    def test_device_delete(self):
        sample_device_refbook()
        res = self.client.delete(DEVICE_URL + '?device=1')
        devices = NotificationDevice.objects.all()
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(devices), 2)
