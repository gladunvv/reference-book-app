from rest_framework import serializers

from refbook.models import NotificationDevice


class DeviceSerializer(serializers.ModelSerializer):

    class Meta:
        model = NotificationDevice
        fields = (
            'id',
            'device_type',
            'address',
            'latitude',
            'longitude',
            'sound_cover_zone',
        )
