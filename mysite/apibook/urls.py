from django.urls import path

from apibook.views import (
    DeviceView,
    SearchDeviceView,
    FilterDeviceView,
)

app_name = 'apibook'

urlpatterns = [
    path('search', SearchDeviceView.as_view(), name='search'),
    path('filter', FilterDeviceView.as_view(), name='filter'),
    path('device', DeviceView.as_view(), name='device'),
]
