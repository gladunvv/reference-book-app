from django.shortcuts import get_object_or_404

from rest_framework import permissions
from rest_framework import generics
from rest_framework import filters
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django_filters import rest_framework as filter_rest

from refbook.models import NotificationDevice

from apibook.serializers import DeviceSerializer


class SearchDeviceView(generics.ListAPIView):
    """
    Поиск утсройств по адресу, широте и долготе.
    """
    permission_classes = (permissions.AllowAny,)
    filter_backends = (filters.SearchFilter,)
    search_fields = ('address', 'latitude', 'longitude')
    queryset = NotificationDevice.objects.all()
    serializer_class = DeviceSerializer


class FilterDeviceView(generics.ListAPIView):
    """
    Фильтрация устройств девайсов по типу, адресу, зоне звукового погрытия.
    """
    permission_classes = (permissions.AllowAny,)
    filter_backends = (filter_rest.DjangoFilterBackend,)
    filterset_fields = ('device_type', 'address', 'sound_cover_zone')
    queryset = NotificationDevice.objects.all()
    serializer_class = DeviceSerializer


class DeviceView(APIView):
    """
    Полный CRUD для устройств.
    """
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        device = NotificationDevice.objects.all()
        serializer = DeviceSerializer(device, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = DeviceSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        device = request.GET.get('device', None)
        if not device:
            return Response({'errors': 'No device provided'}, status=status.HTTP_400_BAD_REQUEST)
        try:
            item = get_object_or_404(NotificationDevice, pk=device)
        except (TypeError, ValueError):
            return Response({'errors': 'Argument must be int'}, status=status.HTTP_400_BAD_REQUEST)
        data = request.data
        serializer = DeviceSerializer(item, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        device = request.GET.get('device', None)
        if not device:
            return Response({'errors': 'No device provided'}, status=status.HTTP_400_BAD_REQUEST)
        try:
            item = get_object_or_404(NotificationDevice, pk=device)
        except (TypeError, ValueError):
            return Response({'errors': 'Argument must be int'}, status=status.HTTP_400_BAD_REQUEST)
        item.delete()
        message = {
            'message': 'Device {type} with longitude: {longitude} and latitude {latitude} deleted successfully'.format(
                type=item.device_type,
                longitude=item.longitude,
                latitude=item.latitude,
            ),
        }
        return Response(message, status=status.HTTP_200_OK)
