# Generated by Django 3.0.3 on 2020-02-29 13:52

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='NotificationDevice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('device_type', models.IntegerField(choices=[(1, 'Сирена'), (2, 'Громкоговоритель')])),
                ('address', models.CharField(max_length=255)),
                ('latitude', models.DecimalField(decimal_places=6, max_digits=9)),
                ('longitude', models.DecimalField(decimal_places=6, max_digits=9)),
                ('sound_cover_zone', models.IntegerField()),
            ],
            options={
                'verbose_name': 'Notification Device',
                'verbose_name_plural': 'Notifications Devices',
                'ordering': ['-id'],
            },
        ),
    ]
