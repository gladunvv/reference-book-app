from django import forms

from refbook.models import NotificationDevice


class DeviceForm(forms.ModelForm):
    """
        Форма добавления устройства.
    """
    class Meta:
        model = NotificationDevice
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(DeviceForm, self).__init__(*args, **kwargs)
        self.fields['device_type'].label = "Тип девайса"
        self.fields['address'].label = "Адресс размещения"
        self.fields['latitude'].label = "Широта"
        self.fields['longitude'].label = "Долгота"
        self.fields['sound_cover_zone'].label = "Радиус зоны звукопокрытия"
