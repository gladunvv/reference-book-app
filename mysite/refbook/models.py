from django.db import models


class NotificationDevice(models.Model):

    TYPE_CHOICE = (
        (1, 'Сирена'),
        (2, 'Громкоговоритель'),
    )
    device_type = models.IntegerField(choices=TYPE_CHOICE)
    address = models.CharField(max_length=255)
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    sound_cover_zone = models.IntegerField()

    class Meta:
        ordering = ['-id']
        verbose_name = 'Notification Device'
        verbose_name_plural = 'Notifications Devices'

    def __str__(self):
        return f'Девайс: {self.get_device_type_display()} Адресс: {self.address}'
