from django.urls import reverse
from django.test import TestCase, Client

from refbook.models import NotificationDevice


DEVICE_LIST_URL = reverse('refbook:device_list')
CREATE_DEVICE_URL = reverse('refbook:create_device')


class DeviceTests(TestCase):

    def setUp(self):
        self.client = Client()

    def test_nd_models_str_method(self):
        context = {
            'device_type': 1,
            'address': 'address',
            'latitude': 1.123123,
            'longitude': 1.123123,
            'sound_cover_zone': 123
        }
        device = NotificationDevice.objects.create(**context)
        self.assertEqual(str(device), 'Девайс: Сирена Адресс: address')

    def test_device_list_get(self):
        res = self.client.get(DEVICE_LIST_URL)
        self.assertEqual(res.status_code, 200)

    def test_create_device_get(self):
        res = self.client.get(CREATE_DEVICE_URL)
        self.assertEqual(res.status_code, 200)

    def test_create_device(self):
        context = {
            'device_type': 1,
            'address': 'addressUNIQ',
            'latitude': 1.123123,
            'longitude': 1.123123,
            'sound_cover_zone': 123
        }
        self.client.post(CREATE_DEVICE_URL, context, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        device = NotificationDevice.objects.get(address='addressUNIQ')
        self.assertEqual(device.sound_cover_zone, 123)

    def test_create_bad_latitude(self):
        context = {
            'device_type': 1,
            'address': 'addressUNIQ',
            'latitude': 1.123123111,
            'longitude': 1.123123,
            'sound_cover_zone': 123
        }
        res = self.client.post(CREATE_DEVICE_URL, context, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(res.status_code, 400)

    def test_create_bad_sound_cover(self):
        context = {
            'device_type': 1,
            'address': 'addressUNIQ',
            'latitude': 1.123123,
            'longitude': 1.123123,
            'sound_cover_zone': 'afds'
        }
        res = self.client.post(CREATE_DEVICE_URL, context, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(res.status_code, 400)
