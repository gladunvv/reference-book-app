from django.urls import path
from refbook.views import DevicesListView, CeateDeviceView

app_name = 'refbook'

urlpatterns = [
    path('', DevicesListView.as_view(), name='device_list'),
    path('refbook-create', CeateDeviceView.as_view(), name='create_device'),
]
