from django.views.generic import ListView
from django.views.generic.edit import CreateView
from django.http import HttpResponseRedirect, JsonResponse

from refbook.forms import DeviceForm
from refbook.models import NotificationDevice


class DevicesListView(ListView):
    """
    Список всех устройств с пагинацией по 4.
    """
    model = NotificationDevice
    paginate_by = 4
    context_object_name = 'devices'
    template_name = 'refbook/list_devices.html'


class CeateDeviceView(CreateView):
    """
    Ajax валидация формы. Добавляет новое устройство в базу данных в случае успеха.
    """
    form_class = DeviceForm
    template_name = 'refbook/create_device.html'

    def post(self, request, *args, **kwargs):
        data = {}
        if request.is_ajax():
            form = self.get_form()
            if form.is_valid():
                form.save()
                data['status'] = 'ok'
                return JsonResponse(data)

            data['status'] = 'error'
            data['errors'] = form.errors
            return JsonResponse(data, status=400)

        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
