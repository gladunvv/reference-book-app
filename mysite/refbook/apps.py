from django.apps import AppConfig


class RefbookConfig(AppConfig):
    name = 'refbook'
